//MainWindow.qml.cs
using Qml.Net;
using System;
using System.Threading.Tasks;
using Ssepan.Application.Core;

namespace QmlNetApp1
{
	[Signal("showFileDialog", NetVariantType.Object)]
    [Signal("showFontDialog", NetVariantType.Object)]
    [Signal("showColorDialog", NetVariantType.Object)]
    [Signal("showMessageDialog", NetVariantType.Object)]
    [Signal("showAboutDialog", NetVariantType.Object)]
    // [Signal("showPrinterDialog", NetVariantType.Object)]
    [Signal("propertyChanged", NetVariantType.String)]
    public class MainWindowCode //: 
        // using Qml Signals instead
        //INotifyPropertyChanged
    {
        private const string APP_NAME = "QmlNetApp1";
        private const string ACTION_IN_PROGRESS = " ...";
        private const string ACTION_CANCELLED = " cancelled";
        private const string ACTION_DONE = " done";

        // /// <summary>
        // /// Methods can return .NET types.
        // /// The returned type can be invoked from Qml (properties/methods/events/etc), 
        // ///  even if it has not been registered.
        // /// Not used in this implementation.
        // /// </summary>
        // /// <returns></returns>
        // public MainWindowCode CreateNetObject()
        // {
        //     return new MainWindowCode();
        // }

        /// <summary>
        /// Qml can pass .NET types to .NET methods.
        /// </summary>
        /// <param name="sender">object</param>
        #region Events
        #region Menu/Toolbar
        public async void FileNew_Triggered(object sender)
        {
            //TODO:call vm methods for statusbar fields, and wire those fields up in qml file
            //Console.WriteLine("FileNew_Triggered"+sender.ToString());
            SetStatusMessage("New" + ACTION_IN_PROGRESS);
            StartProgressBar();
            StartActionIcon("New");
            await FileNewAction();
            StopActionIcon();
            StopProgressBar();
            SetStatusMessage(GetStatusMessage() + ACTION_DONE);
        }
        public async void FileOpen_Triggered(object sender)
        {
            await FileOpenAction();
        }
        public async void FileSave_Triggered(object sender)
        {
            await FileSaveAction();
        }
        public async void FileSaveAs_Triggered(object sender)
        {
            await FileSaveAsAction();
        }
        public async void FilePrint_Triggered(object sender)
        {
            await FilePrintAction();
        }
        public async void FilePrintPreview_Triggered(object sender)
        {
            SetStatusMessage("Print Preview" + ACTION_IN_PROGRESS);
            StartProgressBar();
            //StartActionIcon("Print Preview");
            await FilePrintPreviewAction();
            //StopActionIcon(); 
            StopProgressBar();
            SetStatusMessage(GetStatusMessage() + ACTION_DONE);
        }
        public void FileQuit_Triggered(object sender)
        {
            //TODO:Close() should be called in qml; this, or related action, might be called to do checks and return bool
            //Console.WriteLine("MenuFileQuit_Triggered");
        }
        public async void EditUndo_Triggered(object sender)
        {
            SetStatusMessage("Undo" + ACTION_IN_PROGRESS);
            StartProgressBar();
            StartActionIcon("Undo");
            await EditUndoAction();
            StopActionIcon();
            StopProgressBar();
            SetStatusMessage(GetStatusMessage() + ACTION_DONE);
        }
        public async void EditRedo_Triggered(object sender)
        {
            SetStatusMessage("Redo" + ACTION_IN_PROGRESS);
            StartProgressBar();
            StartActionIcon("Redo");
            await EditRedoAction();
            StopActionIcon();
            StopProgressBar();
            SetStatusMessage(GetStatusMessage() + ACTION_DONE);
        }
        public async void EditSelectAll_Triggered(object sender)
        {
            SetStatusMessage("Select All" + ACTION_IN_PROGRESS);
            StartProgressBar();
            // StartActionIcon("Select All");
            await EditSelectAllAction();
            // StopActionIcon(); 
            StopProgressBar();
            SetStatusMessage(GetStatusMessage() + ACTION_DONE);
        }
        public async void EditCut_Triggered(object sender)
        {
            SetStatusMessage("Cut" + ACTION_IN_PROGRESS);
            StartProgressBar();
            StartActionIcon("Cut");
            await EditCutAction();
            StopActionIcon();
            StopProgressBar();
            SetStatusMessage(GetStatusMessage() + ACTION_DONE);
        }
        public async void EditCopy_Triggered(object sender)
        {
            SetStatusMessage("Copy" + ACTION_IN_PROGRESS);
            StartProgressBar();
            StartActionIcon("Copy");
            await EditCopyAction();
            StopActionIcon();
            StopProgressBar();
            SetStatusMessage(GetStatusMessage() + ACTION_DONE);
        }
        public async void EditPaste_Triggered(object sender)
        {
            SetStatusMessage("Paste" + ACTION_IN_PROGRESS);
            StartProgressBar();
            StartActionIcon("Paste");
            await EditPasteAction();
            StopActionIcon();
            StopProgressBar();
            SetStatusMessage(GetStatusMessage() + ACTION_DONE);
        }
        public async void EditPasteSpecial_Triggered(object sender)
        {
            SetStatusMessage("Paste Special" + ACTION_IN_PROGRESS);
            StartProgressBar();
            await EditPasteSpecialAction();
            StopProgressBar();
            SetStatusMessage(GetStatusMessage() + ACTION_DONE);
        }
        public async void EditDelete_Triggered(object sender)
        {
            SetStatusMessage("Delete" + ACTION_IN_PROGRESS);
            StartProgressBar();
            StartActionIcon("Delete");
            await EditDeleteAction();
            StopActionIcon();
            StopProgressBar();
            SetStatusMessage(GetStatusMessage() + ACTION_DONE);
        }
        public async void EditFind_Triggered(object sender)
        {
            SetStatusMessage("Find" + ACTION_IN_PROGRESS);
            StartProgressBar();
            StartActionIcon("Find");
            await EditFindAction();
            StopActionIcon();
            StopProgressBar();
            SetStatusMessage(GetStatusMessage() + ACTION_DONE);
        }
        public async void EditReplace_Triggered(object sender)
        {
            SetStatusMessage("Replace" + ACTION_IN_PROGRESS);
            StartProgressBar();
            StartActionIcon("Replace");
            await EditReplaceAction();
            StopActionIcon();
            StopProgressBar();
            SetStatusMessage(GetStatusMessage() + ACTION_DONE);
        }
        public async void EditRefresh_Triggered(object sender)
        {
            SetStatusMessage("Refresh" + ACTION_IN_PROGRESS);
            StartProgressBar();
            StartActionIcon("Reload");
            await EditRefreshAction();
            StopActionIcon();
            StopProgressBar();
            SetStatusMessage(GetStatusMessage() + ACTION_DONE);
        }
        public async void EditPreferences_Triggered(object sender)
        {
            await EditPreferencesAction();
        }
        public async void EditProperties_Triggered(object sender)
        {
            SetStatusMessage("Properties" + ACTION_IN_PROGRESS);
            StartProgressBar();
            StartActionIcon("Properties");
            await EditPropertiesAction();
            StopActionIcon();
            StopProgressBar();
            SetStatusMessage(GetStatusMessage() + ACTION_DONE);
        }
        public async void WindowNewWindow_Triggered(object sender)
        {
            SetStatusMessage("New Window" + ACTION_IN_PROGRESS);
            StartProgressBar();
            await WindowNewWindowAction();
            StopActionIcon();
            StopProgressBar();
            SetStatusMessage(GetStatusMessage() + ACTION_DONE);
        }
        public async void WindowTile_Triggered(object sender)
        {
            SetStatusMessage("Tile" + ACTION_IN_PROGRESS);
            StartProgressBar();
            await WindowTileAction();
            StopActionIcon();
            StopProgressBar();
            SetStatusMessage(GetStatusMessage() + ACTION_DONE);
        }
        public async void WindowCascade_Triggered(object sender)
        {
            SetStatusMessage("Cascade" + ACTION_IN_PROGRESS);
            StartProgressBar();
            await WindowCascadeAction();
            StopActionIcon();
            StopProgressBar();
            SetStatusMessage(GetStatusMessage() + ACTION_DONE);
        }
        public async void WindowArrangeAll_Triggered(object sender)
        {
            SetStatusMessage("Arrange All" + ACTION_IN_PROGRESS);
            StartProgressBar();
            await WindowArrangeAllAction();
            StopActionIcon();
            StopProgressBar();
            SetStatusMessage(GetStatusMessage() + ACTION_DONE);
        }
        public async void WindowHide_Triggered(object sender)
        {
            SetStatusMessage("Hide" + ACTION_IN_PROGRESS);
            StartProgressBar();
            await WindowHideAction();
            StopActionIcon();
            StopProgressBar();
            SetStatusMessage(GetStatusMessage() + ACTION_DONE);
        }
        public async void WindowShow_Triggered(object sender)
        {
            SetStatusMessage("Show" + ACTION_IN_PROGRESS);
            StartProgressBar();
            await WindowShowAction();
            StopActionIcon();
            StopProgressBar();
            SetStatusMessage(GetStatusMessage() + ACTION_DONE);
        }
        public async void HelpContents_Triggered(object sender)
        {
            SetStatusMessage("Contents" + ACTION_IN_PROGRESS);
            StartProgressBar();
            StartActionIcon("Contents"); 
            await HelpContentsAction();
            StopActionIcon();
            StopProgressBar();
            SetStatusMessage(GetStatusMessage() + ACTION_DONE);
        }
        public async void HelpIndex_Triggered(object sender)
        {
            SetStatusMessage("Index" + ACTION_IN_PROGRESS);
            StartProgressBar();
            // StartActionIcon("Index"); 
            await HelpIndexAction();
            // StopActionIcon(); 
            StopProgressBar();
            SetStatusMessage(GetStatusMessage() + ACTION_DONE);
        }
        public async void HelpOnlineHelp_Triggered(object sender)
        {
            SetStatusMessage("Online Help" + ACTION_IN_PROGRESS);
            StartProgressBar();
            // StartActionIcon("Online Help"); 
            await HelpOnlineHelpAction();
            // StopActionIcon(); 
            StopProgressBar();
            SetStatusMessage(GetStatusMessage() + ACTION_DONE);
        }
        public async void HelpLicenceInformation_Triggered(object sender)
        {
            SetStatusMessage("Licence Information" + ACTION_IN_PROGRESS);
            StartProgressBar();
            // StartActionIcon("Licence Information"); 
            await HelpLicenceInformationAction();
            // StopActionIcon(); 
            StopProgressBar();
            SetStatusMessage(GetStatusMessage() + ACTION_DONE);
        }
        public async void HelpCheckForUpdates_Triggered(object sender)
        {
            SetStatusMessage("Check For Updates" + ACTION_IN_PROGRESS);
            StartProgressBar();
            await HelpCheckForUpdatesAction();
            StopProgressBar();
            SetStatusMessage(GetStatusMessage() + ACTION_DONE);
        }
        public async void HelpAbout_Triggered(object sender)
        {
            await HelpAboutAction();
        }
        #endregion Menu/Toolbar

        #region Buttons
        public void CmdRun_Clicked(object sender)
        {
            RunAction();
        }
        public async void CmdFont_Clicked(object sender)
        {
            await FontAction();
        }
        public async void CmdColor_Clicked(object sender)
        {
            await ColorAction();
        }
        #endregion Buttons
        #endregion Events

        #region DialogInfo
        protected void OnShowMessageDialog(string dialogInfoType)
        {
            try
            {
                // Console.WriteLine("OnShowMessageDialog:" + dialogInfoType);
                //qml file will be watching for onShowMessageDialog
                this.ActivateSignal("showMessageDialog", dialogInfoType);
            }
            catch (Exception ex)
            {
                Console.Error.WriteLine(ex.Message);
            }
        }
        protected void OnShowAboutDialog(string dialogInfoType)
        {
            try
            {
                //Console.WriteLine("OnShowAboutDialog:" + dialogInfoType);
                //qml file will be watching for onShowAboutDialog
                this.ActivateSignal("showAboutDialog", dialogInfoType);
            }
            catch (Exception ex)
            {
                Console.Error.WriteLine(ex.Message);
            }
        }
        protected void OnShowFontDialog(string dialogInfoType)
        {
            try
            {
                //Console.WriteLine("OnShowFontDialog:" + dialogInfoType);
                //qml file will be watching for onShowFontDialog
                this.ActivateSignal("showFontDialog", dialogInfoType);
            }
            catch (Exception ex)
            {
                Console.Error.WriteLine(ex.Message);
            }
        }
        protected void OnShowColorDialog(string dialogInfoType)
        {
            try
            {
                //Console.WriteLine("OnShowColorDialog:" + dialogInfoType);
                //qml file will be watching for onShowColorDialog
                this.ActivateSignal("showColorDialog", dialogInfoType);
            }
            catch (Exception ex)
            {
                Console.Error.WriteLine(ex.Message);
            }
        }
        protected void OnShowFileDialog(string dialogInfoType)
        {
            try
            {
                //Console.WriteLine("OnShowFileDialog:" + dialogInfoType);
                //qml file will be watching for onShowFileDialog
                this.ActivateSignal("showFileDialog", dialogInfoType);
            }
            catch (Exception ex)
            {
                Console.Error.WriteLine(ex.Message);
            }
        }
        // protected void OnShowPrinterDialog(string dialogInfoType)
        // {
        //     try
        //     {
        //         //Console.WriteLine("OnShowPrinterDialog:" + dialogInfoType);
        //         //qml file will be watching for onShowPrinterDialog
        //         this.ActivateSignal("showPrinterDialog", dialogInfoType);
        //     }
        //     catch (Exception ex)
        //     {
        //         Console.Error.WriteLine(ex.Message);
        //     }
        // }
        #endregion DialogInfo

        #region Model
        #region INotifyPropertyChanged 
        //If property of object changes, call OnPropertyChanged, which notifies any subscribed observers by firing PropertyChanged.
        //Called by all 'set' statements in IModelComponent object properties.
        //public event PropertyChangedEventHandler PropertyChanged;
        //Implementing INotifyPropertyChanged was mostly an exercise in organizing the class as desired; 
        // this method will raise a Qml Signal
        protected void OnPropertyChanged(string propertyName)
        {
            try
            {
                //if (this.PropertyChanged != null)
                //{
                    //Console.WriteLine("OnPropertyChanged:"+propertyName);
                    //this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));//Note: this notification not likely to actually be watched, ...
                    this.ActivateSignal("propertyChanged", propertyName);//Note: ...qml file will be watching for this notification instead.
                //}
            }
            catch (Exception ex)
            {
                Console.Error.WriteLine(ex.Message);
            }
        }
        #endregion INotifyPropertyChanged 

        #region Properties
        //Note:normally I wouldn't get this elaborate for the prototype, 
        // but properties and notifications seem to be necessary 
        // for interfacing with Qml / Qml.Net
        #region Fields
        private int _SomeInt;
        public int SomeInt
        {
            get { return _SomeInt; }
            set
            {
                _SomeInt = value;
                OnPropertyChanged(nameof(SomeInt));
                // Console.WriteLine("SomeInt:"+value.ToString());
            }
        }

        private bool _SomeBoolean;
        public bool SomeBoolean
        {
            get { return _SomeBoolean; }
            set
            {
                _SomeBoolean = value;
                OnPropertyChanged(nameof(SomeBoolean));
                // Console.WriteLine("SomeBoolean:"+value.ToString());
            }
        }

        private string _SomeString = string.Empty;
        public string SomeString
        {
            get { return _SomeString; }
            set
            {
                _SomeString = value;
                OnPropertyChanged(nameof(SomeString));
                // Console.WriteLine("SomeString:"+value.ToString());
            }
        }

        private int _SomeOtherInt;
        public int SomeOtherInt
        {
            get { return _SomeOtherInt; }
            set
            {
                _SomeOtherInt = value;
                OnPropertyChanged(nameof(SomeOtherInt));
                // Console.WriteLine("SomeOtherInt:"+value.ToString());
            }
        }

        private bool _SomeOtherBoolean;
        public bool SomeOtherBoolean
        {
            get { return _SomeOtherBoolean; }
            set
            {
                _SomeOtherBoolean = value;
                OnPropertyChanged(nameof(SomeOtherBoolean));
                // Console.WriteLine("SomeOtherBoolean:"+value.ToString());
            }
        }

        private string _SomeOtherString = string.Empty;
        public string SomeOtherString
        {
            get { return _SomeOtherString; }
            set
            {
                _SomeOtherString = value;
                OnPropertyChanged(nameof(SomeOtherString));
                // Console.WriteLine("SomeOtherString:"+value.ToString());
            }
        }

        private int _StillAnotherInt;
        public int StillAnotherInt
        {
            get { return _StillAnotherInt; }
            set
            {
                _StillAnotherInt = value;
                OnPropertyChanged(nameof(StillAnotherInt));
                // Console.WriteLine("StillAnotherInt:"+value.ToString());
            }
        }

        private bool _StillAnotherBoolean;
        public bool StillAnotherBoolean
        {
            get { return _StillAnotherBoolean; }
            set
            {
                _StillAnotherBoolean = value;
                OnPropertyChanged(nameof(StillAnotherBoolean));
                // Console.WriteLine("StillAnotherBoolean:"+value.ToString());
            }
        }

        private string _StillAnotherString = string.Empty;
        public string StillAnotherString
        {
            get { return _StillAnotherString; }
            set
            {
                _StillAnotherString = value;
                OnPropertyChanged(nameof(StillAnotherString));
                // Console.WriteLine("StillAnotherString:"+value.ToString());
            }
        }
        #endregion Fields
        #endregion Properties
        #endregion Model

        #region ViewModel
        #region Properties
        private string _StatusMessage;
        public string StatusMessage
        {
            get { return _StatusMessage; }
            set
            {
                if (value != _StatusMessage)
                {
                    _StatusMessage = value;
                    OnPropertyChanged(nameof(StatusMessage));
                }
            }
        }

        private string _ErrorMessage;
        public string ErrorMessage
        {
            get { return _ErrorMessage; }
            set
            {
                if (value != _ErrorMessage)
                {
                    _ErrorMessage = value;
                    OnPropertyChanged(nameof(ErrorMessage));
                }
            }
        }

        private string _CustomMessage;
        public string CustomMessage
        {
            get { return _CustomMessage; }
            set
            {
                if (value != _CustomMessage)
                {
                    _CustomMessage = value;
                    OnPropertyChanged(nameof(CustomMessage));
                }
            }
        }

        private string _ErrorMessageToolTipText;
        public string ErrorMessageToolTipText
        {
            get { return _ErrorMessageToolTipText; }
            set
            {
                if (value != _ErrorMessageToolTipText)
                {
                    _ErrorMessageToolTipText = value;
                    OnPropertyChanged(nameof(ErrorMessageToolTipText));
                }
            }
        }

        private int _ProgressBarValue;
        public int ProgressBarValue
        {
            get { return _ProgressBarValue; }
            set
            {
                if (value != _ProgressBarValue)
                {
                    _ProgressBarValue = value;
                    OnPropertyChanged(nameof(ProgressBarValue));
                }
            }
        }

        private int _ProgressBarMaximum;
        public int ProgressBarMaximum
        {
            get { return _ProgressBarMaximum; }
            set
            {
                if (value != _ProgressBarMaximum)
                {
                    _ProgressBarMaximum = value;
                    OnPropertyChanged(nameof(ProgressBarMaximum));
                }
            }
        }

        private int _ProgressBarMinimum;
        public int ProgressBarMinimum
        {
            get { return _ProgressBarMinimum; }
            set
            {
                if (value != _ProgressBarMinimum)
                {
                    _ProgressBarMinimum = value;
                    OnPropertyChanged(nameof(ProgressBarMinimum));
                }
            }
        }

        private int _ProgressBarStep;
        public int ProgressBarStep
        {
            get { return _ProgressBarStep; }
            set
            {
                if (value != _ProgressBarStep)
                {
                    _ProgressBarStep = value;
                    OnPropertyChanged(nameof(ProgressBarStep));
                }
            }
        }

        private bool _ProgressBarIsMarquee;
        public bool ProgressBarIsMarquee
        {
            get { return _ProgressBarIsMarquee; }
            set
            {
                if (value != _ProgressBarIsMarquee)
                {
                    _ProgressBarIsMarquee = value;
                    OnPropertyChanged(nameof(ProgressBarIsMarquee));
                }
            }
        }

        private bool _ProgressBarIsVisible;
        public bool ProgressBarIsVisible
        {
            get { return _ProgressBarIsVisible; }
            set
            {
                if (value != _ProgressBarIsVisible)
                {
                    _ProgressBarIsVisible = value;
                    OnPropertyChanged(nameof(ProgressBarIsVisible));
                }
            }
        }

        private bool _ActionIconIsVisible;
        public bool ActionIconIsVisible
        {
            get { return _ActionIconIsVisible; }
            set
            {
                if (value != _ActionIconIsVisible)
                {
                    _ActionIconIsVisible = value;
                    OnPropertyChanged(nameof(ActionIconIsVisible));
                }
            }
        }

        private string _ActionIconImage;
        public string ActionIconImage
        {
            get { return _ActionIconImage; }
            set
            {
                if (value != _ActionIconImage)
                {
                    _ActionIconImage = value;
                    OnPropertyChanged(nameof(ActionIconImage));
                }
            }
        }

        private bool _DirtyIconIsVisible;
        public bool DirtyIconIsVisible
        {
            get { return _DirtyIconIsVisible; }
            set
            {
                if (value != _DirtyIconIsVisible)
                {
                    _DirtyIconIsVisible = value;
                    OnPropertyChanged(nameof(DirtyIconIsVisible));
                }
            }
        }

        private string _DirtyIconImage;
        public string DirtyIconImage
        {
            get { return _DirtyIconImage; }
            set
            {
                if (value != _DirtyIconImage)
                {
                    _DirtyIconImage = value;
                    OnPropertyChanged(nameof(DirtyIconImage));
                }
            }
        }

		#region DialogInfo
		public MessageDialogInfo<object, string, object, string, string> MsgDialogInfo { get; set; }
		public AboutDialogInfo<object, string, string> AbtDialogInfo { get; set; }
		public FontDialogInfo<object, string, string> FntDialogInfo { get; set; }
		public ColorDialogInfo<object, string, string> ClrDialogInfo { get; set; }
		public FileDialogInfo<object, string> FilDialogInfo { get; set; }
		public PrinterDialogInfo<object, string, object> PrtDialogInfo { get; set; }
		#endregion DialogInfo
		#endregion Properties

		#region Methods
		#region Actions
		private static async Task DoSomething()
        {
            for (int i = 0; i < 3; i++)
            {
                //TODO:DoEvents...
                await Task.Delay(1000);
            }
        }
        private void RunAction()
        {
            try
            {
                SetStatusMessage("Run" + ACTION_IN_PROGRESS);
                StartProgressBar();

				SomeInt++;
                SomeString = DateTime.Now.ToString();
                SomeBoolean = !SomeBoolean;

				SomeOtherInt++;
                SomeOtherString = DateTime.Now.ToString();
                SomeOtherBoolean = !SomeOtherBoolean;

				StillAnotherInt++;
                StillAnotherString = DateTime.Now.ToString();
                StillAnotherBoolean = !StillAnotherBoolean;
                //Console.WriteLine("RunAction()");

                StopProgressBar();
                SetStatusMessage(GetStatusMessage() + ACTION_DONE);
            }
            catch (Exception ex)
            {
                Console.Error.WriteLine(ex.Message);
            }
        }
        private async Task FontAction()
        {
            try
            {
                SetStatusMessage("Font" + ACTION_IN_PROGRESS);
                StartProgressBar();

				//Console.WriteLine("FontAction()");
				// Use custom signal to invoke dialog in Qml, 
				// passing dialogInfo and awaiting for semaphore inside same when finished.
				FntDialogInfo = new FontDialogInfo<object, string, string>
				{
					Title = null,
					FontDescription = null,
					Busy = true
				};
				OnShowFontDialog("Font");
                await WaitDialogFree(FntDialogInfo);
                //Console.WriteLine("FntDialogInfo.Response:" + FntDialogInfo.Response);

                if (!string.IsNullOrEmpty(FntDialogInfo.Response))
                {
                    if (FntDialogInfo.Response == "Ok")
                    {
                        StopProgressBar();
                        SetStatusMessage(GetStatusMessage() + FntDialogInfo.FontDescription + ACTION_IN_PROGRESS + ACTION_DONE);
                    }
                    else
                    {
                        StopProgressBar();
                        SetStatusMessage(GetStatusMessage() + ACTION_CANCELLED + ACTION_IN_PROGRESS + ACTION_DONE);
                    }
                }
                else
                {
                    throw new Exception("FontAction:FntDialogInfo.Response:"+FntDialogInfo.Response);
                }
            }
            catch (Exception ex)
            {
                Console.Error.WriteLine(ex.Message);
            }
        }
        private async Task ColorAction()
        {
            try
            {
                SetStatusMessage("Color" + ACTION_IN_PROGRESS);
                StartProgressBar();

				//Console.WriteLine("ColorAction()");
				// Use custom signal to invoke dialog in Qml, 
				// passing dialogInfo and awaiting for semaphore inside same when finished.
				ClrDialogInfo = new ColorDialogInfo<object, string, string>
				{
					Title = null,
					Color = null,
					Busy = true
				};
				OnShowColorDialog("Color");
                await WaitDialogFree(ClrDialogInfo);
                //Console.WriteLine("ClrDialogInfo.Response:" + ClrDialogInfo.Response);

                if (!string.IsNullOrEmpty(ClrDialogInfo.Response))
                {
                    if (ClrDialogInfo.Response == "Ok")
                    {
                        StopProgressBar();
                        SetStatusMessage(GetStatusMessage() + ClrDialogInfo.Color + ACTION_IN_PROGRESS + ACTION_DONE);
                    }
                    else
                    {
                        StopProgressBar();
                        SetStatusMessage(GetStatusMessage() + ACTION_CANCELLED + ACTION_IN_PROGRESS + ACTION_DONE);
                    }
                }
                else
                {
                    throw new Exception("ColorAction:ClrDialogInfo.Response:"+ClrDialogInfo.Response);
                }
            }
            catch (Exception ex)
            {
                Console.Error.WriteLine(ex.Message);
            }
        }
        private static async Task FileNewAction()
        {
            await DoSomething();
        }
        private async Task FileOpenAction()
        {
            try
            {
                SetStatusMessage("Open" + ACTION_IN_PROGRESS);
                StartProgressBar();
                StartActionIcon("Open");

				//Console.WriteLine("FileOpenAction()");
				// Use custom signal to invoke dialog in Qml, 
				// passing dialogInfo and awaiting for semaphore inside same when finished.
				FilDialogInfo =
					new FileDialogInfo<object, string>
					{
						Title = "Open - " + APP_NAME,
						Filename = null,
						MustExist = true,
						Filters =
							string.Join
							(
								FileDialogInfo<object, string>.FILTER_SEPARATOR,
								[
                                    // "MvcSettings files (*.mvcsettings)", 
                                    // "JSON files (*.json)", 
                                    // "XML files (*.xml)", 
                                    // "All files (*.*)" 
                                    string.Format(FileDialogInfo<object, string>.FILTER_FORMAT, "MvcSettings files", "mvcsettings"),
									string.Format(FileDialogInfo<object, string>.FILTER_FORMAT, "JSON files", "json"),
									string.Format(FileDialogInfo<object, string>.FILTER_FORMAT, "XML files", "xml"),
									string.Format(FileDialogInfo<object, string>.FILTER_FORMAT, "All files", "*")//, 
                                ]
							),
						Busy = true
					};
				OnShowFileDialog("File");
                await WaitDialogFree(FilDialogInfo);
                //Console.WriteLine("FilDialogInfo.Response:" + FilDialogInfo.Response);

                if (!string.IsNullOrEmpty(FilDialogInfo.Response))
                {
                    if (FilDialogInfo.Response == "Ok")
                    {
                        //return Value from dialog info and display in status

                        StopActionIcon();
                        StopProgressBar();
                        SetStatusMessage(GetStatusMessage() + "'" + FilDialogInfo.Filename + "'" + ACTION_IN_PROGRESS + ACTION_DONE);
                    }
                    else
                    {
                        StopActionIcon();
                        StopProgressBar();
                        SetStatusMessage(GetStatusMessage() + ACTION_CANCELLED + ACTION_IN_PROGRESS + ACTION_DONE);
                    }
                }
                else
                {
                    throw new Exception("FileOpenAction:FilDialogInfo.Response:"+FilDialogInfo.Response);
                }
            }
            catch (Exception ex)
            {
                Console.Error.WriteLine(ex.Message);
            }
        }
        private async Task FileSaveAction(bool isSaveAs=false)
        {
            try
            {
                SetStatusMessage((isSaveAs ? "Save As" : "Save") + ACTION_IN_PROGRESS);
                StartProgressBar();
                StartActionIcon("Save");

				//Console.WriteLine("FileSaveAction()");
				// Use custom signal to invoke dialog in Qml, 
				// passing dialogInfo and awaiting for semaphore inside same when finished.
				FilDialogInfo =
					new FileDialogInfo<object, string>
					{
						Title = "Save - " + APP_NAME,
						Filename = null,
						MustExist = false,
						Filters =
						string.Join
						(
							FileDialogInfo<object, string>.FILTER_SEPARATOR,
							[
                                    // "MvcSettings files (*.mvcsettings)", 
                                    // "JSON files (*.json)", 
                                    // "XML files (*.xml)", 
                                    // "All files (*.*)" 
                                    string.Format(FileDialogInfo<object, string>.FILTER_FORMAT, "MvcSettings files", "mvcsettings"),
									string.Format(FileDialogInfo<object, string>.FILTER_FORMAT, "JSON files", "json"),
									string.Format(FileDialogInfo<object, string>.FILTER_FORMAT, "XML files", "xml"),
									string.Format(FileDialogInfo<object, string>.FILTER_FORMAT, "All files", "*")//, 
                            ]
						),
						Busy = true
					};
				OnShowFileDialog("File");
                await WaitDialogFree(FilDialogInfo);
                //Console.WriteLine("FilDialogInfo.Response:" + FilDialogInfo.Response);

                if (!string.IsNullOrEmpty(FilDialogInfo.Response))
                {
                    if (FilDialogInfo.Response == "Ok")
                    {
                        //return Value from dialog info and display in status

                        StopActionIcon();
                        StopProgressBar();
                        SetStatusMessage(GetStatusMessage() + "'" + FilDialogInfo.Filename + "'" + ACTION_IN_PROGRESS + ACTION_DONE);
                    }
                    else
                    {
                        StopActionIcon();
                        StopProgressBar();
                        SetStatusMessage(GetStatusMessage() + ACTION_CANCELLED + ACTION_IN_PROGRESS + ACTION_DONE);
                    }
                }
                else
                {
                    throw new Exception("FileSaveAction:FilDialogInfo.Response:"+FilDialogInfo.Response);
                }
            }
            catch (Exception ex)
            {
                Console.Error.WriteLine(ex.Message);
            }
        }
        private async Task  FileSaveAsAction()
        {
            await FileSaveAction(true);
        }
        private async Task  FilePrintAction()
        {
            try
            {
                SetStatusMessage("Print" + ACTION_IN_PROGRESS);
                StartProgressBar();
                StartActionIcon("Print");

                //Console.WriteLine("FilePrintAction()");
                // Use custom signal to invoke dialog in Qml, 
                // passing dialogInfo and awaiting for semaphore inside same when finished.
                // PrtDialogInfo = 
                //     new FileDialogInfo<object, string>()
                //     {
                //         Title = "Print" + " - " + APP_NAME, 
                //         Printer = null,
                //     };
                await DoSomething();
                // // PrtDialogInfo.Busy = true;
                // // OnShowFileDialog("Print");
                // // await WaitDialogFree(PrtDialogInfo);
                // // Console.WriteLine("PrtDialogInfo.Response:" + PrtDialogInfo.Response);

                // if (!string.IsNullOrEmpty(PrtDialogInfo.Response))
                // {
                //     if (PrtDialogInfo.Response == "Ok")
                //     {
                //         //return Value from dialog info and display in status

                //         StopActionIcon(); 
                //         StopProgressBar();
                //         SetStatusMessage(GetStatusMessage() + "'" + PrtDialogInfo.Printer + "'" + ACTION_IN_PROGRESS + ACTION_DONE);
                //     }
                //     else
                //    {
                        StopActionIcon();
                        StopProgressBar();
                        SetStatusMessage(GetStatusMessage() + ACTION_CANCELLED + ACTION_IN_PROGRESS + ACTION_DONE);
                //     }
                // }
                // else
                // {
                //     throw new Exception("FilePrintAction:PrtDialogInfo.Response:"+PrtDialogInfo.Response);
                // }
            }
            catch (Exception ex)
            {
                Console.Error.WriteLine(ex.Message);
            }
        }
        private static async Task  FilePrintPreviewAction()
        {
            await DoSomething();
        }

        /// <summary>
        /// Returns true = Yes = Quit
        /// </summary>
        public async Task<bool> FileQuitAction()
        {
            bool returnValue = false;

            try
            {
                SetStatusMessage("Quit" + ACTION_IN_PROGRESS);
                StartProgressBar();

				// Use custom signal to invoke dialog in Qml, 
				// passing dialogInfo and awaiting for semaphore inside same when finished.
				//Console.WriteLine("FileQuitAction");
				MsgDialogInfo = new MessageDialogInfo<object, string, object, string, string>
				{
					Title = null,
					Message = "Quit?"/*, Message2 =  "informativeText"*//*, Message3 = "detailedText"*/,
					MessageType = "Question",
					ButtonsType = "YesNo",
					Busy = true
				};
				OnShowMessageDialog("Message");
                await WaitDialogFree(MsgDialogInfo);
                //Console.WriteLine("MsgDialogInfo.Response:" + MsgDialogInfo.Response);

                if (!string.IsNullOrEmpty(MsgDialogInfo.Response))
                {
                    if (MsgDialogInfo.Response == "Yes")
                    {
                        returnValue = true;
                        //3) Qml ignores result, so scram
                        QCoreApplication.Quit();
                        StopProgressBar();
                        SetStatusMessage(GetStatusMessage() + ACTION_DONE);
                    }
                    else
                    {
                        StopProgressBar();
                        SetStatusMessage(GetStatusMessage() + ACTION_CANCELLED + ACTION_IN_PROGRESS + ACTION_DONE);
                    }
                }
                else
                {
                    throw new Exception("FileQuitAction:MsgDialogInfo.Response:"+MsgDialogInfo.Response);
                }
            }
            catch (Exception ex)
            {
                Console.Error.WriteLine("Exception: {0}", ex.Message);
            }
            return returnValue;
        }
        private static async Task EditUndoAction()
        {
            await DoSomething();
        }
        private static async Task EditRedoAction()
        {
            await DoSomething();
        }
        private static async Task EditSelectAllAction()
        {
            await DoSomething();
        }
        private static async Task EditCutAction()
        {
            await DoSomething();
        }
        private static async Task EditCopyAction()
        {
            await DoSomething();
        }
        private static async Task EditPasteAction()
        {
            await DoSomething();
        }
        private static async Task EditPasteSpecialAction()
        {
            await DoSomething();
        }
        private static async Task EditDeleteAction()
        {
            await DoSomething();
        }
        private static async Task EditFindAction()
        {
            await DoSomething();
        }
        private static async Task EditReplaceAction()
        {
            await DoSomething();
        }
        private static async Task EditRefreshAction()
        {
            await DoSomething();
        }
        private async Task EditPreferencesAction()
        {
            try
            {
                SetStatusMessage("Preferences" + ACTION_IN_PROGRESS);
                StartProgressBar();
                StartActionIcon("Preferences");

				//Console.WriteLine("EditPreferencesAction()");
				// Use custom signal to invoke dialog in Qml, 
				// passing dialogInfo and awaiting for semaphore inside same when finished.
				FilDialogInfo =
					new FileDialogInfo<object, string>
					{
						Title = "Folder - " + APP_NAME,
						Filename = null,
						MustExist = true,
						Multiselect = false,
						SelectFolders = true,
						Filters =
							string.Join
							(
								FileDialogInfo<object, string>.FILTER_SEPARATOR,
								[
									"All files (*.*)"
                                    // string.Format(FileDialogInfo<object, string>.FILTER_FORMAT, "All files", "*")//, 
                                ]
							),
						Busy = true
					};
				OnShowFileDialog("File");
                await WaitDialogFree(FilDialogInfo);
                //Console.WriteLine("FilDialogInfo.Response:" + FilDialogInfo.Response);

                if (!string.IsNullOrEmpty(FilDialogInfo.Response))
                {
                    if (FilDialogInfo.Response == "Ok")
                    {
                        //return Value from dialog info and display in status

                        StopActionIcon();
                        StopProgressBar();
                        SetStatusMessage(GetStatusMessage() + "'" + FilDialogInfo.Filename + "'" + ACTION_IN_PROGRESS + ACTION_DONE);
                    }
                    else
                    {
                        StopActionIcon();
                        StopProgressBar();
                        SetStatusMessage(GetStatusMessage() + ACTION_CANCELLED + ACTION_IN_PROGRESS + ACTION_DONE);
                    }
                }
                else
                {
                    throw new Exception("EditPreferencesAction:FilDialogInfo.Response:"+FilDialogInfo.Response);
                }
            }
            catch (Exception ex)
            {
                Console.Error.WriteLine(ex.Message);
            }
        }
        private static async Task EditPropertiesAction()
        {
            await DoSomething();
        }
        private static async Task WindowNewWindowAction()
        {
            await DoSomething();
        }
        private static async Task WindowTileAction()
        {
            await DoSomething();
        }
        private static async Task WindowCascadeAction()
        {
            await DoSomething();
        }
        private static async Task WindowArrangeAllAction()
        {
            await DoSomething();
        }
        private static async Task WindowHideAction()
        {
            await DoSomething();
        }
        private static async Task WindowShowAction()
        {
            await DoSomething();
        }
        private static async Task HelpContentsAction()
        {
            await DoSomething();
        }
        private static async Task HelpIndexAction()
        {
            await DoSomething();
        }
        private static async Task HelpOnlineHelpAction()
        {
            await DoSomething();
        }
        private static async Task HelpLicenceInformationAction()
        {
            await DoSomething();
        }
        private static async Task HelpCheckForUpdatesAction()
        {
            await DoSomething();
            // MessageBox.MessageBoxResult messageBoxResult = MessageBox.MessageBoxResult.None;

            // try
            // {
            //     messageBoxResult = MessageBox.MessageBoxResult.None;
            //     messageBoxResult = MessageBox.Show(this, "Check for Updates?", this.Title, MessageBox.MessageBoxButtons.YesNo);
            //     if (messageBoxResult != MessageBox.MessageBoxResult.None)
            //     {
            //         SetStatusMessage(GetStatusMessage() + messageBoxResult.ToString() + ACTION_IN_PROGRESS);
            //     }
            //     else
            //     {
            //         throw new Exception("HelpCheckForUpdatesAction:messageBoxResult:"+messageBoxResult.ToString());
            //     }
            // }
            // catch (Exception ex)
            // {
            //     Console.Error.WriteLine("Exception:", ex.Message);
            // }
        }
        public async Task HelpAboutAction()
        {
            try
            {//TODO:call this through HelpAbout_Triggered()
                SetStatusMessage("About" + ACTION_IN_PROGRESS);
                StartProgressBar();
                StartActionIcon("About");

				// Use custom signal to invoke dialog in Qml, 
				// passing dialogInfo and awaiting for semaphore inside same when finished.
				//Console.WriteLine("HelpAboutAction");
				AbtDialogInfo = new AboutDialogInfo<object, string, string>
				{
					Title = null,
					Comments = "Qml Gui App1\nVersion 0.1\nby Stephen J Sepan\nUsing Qml.Net",
                    /*, Message2 =  "informativeText"*/
                    /*, Message3 = "detailedText"*/
					Busy = true
				};
				OnShowAboutDialog("About");
                await WaitDialogFree(AbtDialogInfo);
                //Console.WriteLine("AbtDialogInfo.Response:" + AbtDialogInfo.Response);

                if (!string.IsNullOrEmpty(AbtDialogInfo.Response))
                {
                    if (AbtDialogInfo.Response == "Ok")
                    {
                        StopActionIcon();
                        StopProgressBar();
                        SetStatusMessage(GetStatusMessage() + ACTION_DONE);
                    }
                    else
                    {
                        StopActionIcon();
                        StopProgressBar();
                        SetStatusMessage(GetStatusMessage() + ACTION_CANCELLED + ACTION_IN_PROGRESS + ACTION_DONE);
                    }
                }
                else
                {
                    throw new Exception("HelpAboutAction:messageBoxResult:"+AbtDialogInfo.Response);
                }
            }
            catch (Exception ex)
            {
                Console.Error.WriteLine("Exception: {0}", ex.Message);
            }
        }
        #endregion Actions

        #region utility
        private string GetStatusMessage()
        {
            return StatusMessage;
        }

        private void SetStatusMessage(string statusMessage)
        {
            StatusMessage = statusMessage;
            //Console.WriteLine("SetStatusMessage"+statusMessage);
        }

        private void SetErrorMessage(string errorMessage)
        {
            ErrorMessage = errorMessage;
        }

        private void StartProgressBar()
        {
            // _progressBar.Fraction = 0.33;
            // _progressBar.PulseStep=0.1;
            // _progressBar.Visible = true;
            // _progressBar.Pulse();
            ProgressBarIsVisible = true;
        }

        private void StopProgressBar()
        {
            ProgressBarIsVisible = false;
        }

        private void StartActionIcon(string actionName)
        {
            ActionIconImage = string.Format("Resources/{0}.png", string.IsNullOrWhiteSpace(actionName) ? "App" : actionName);
            ActionIconIsVisible = true;
        }

        private void StopActionIcon()
        {
            ActionIconIsVisible = false;
            // this.ActionIconImage = null;//string.Format("Resources/{0}.png", "_Blank");
        }

        private void SetDirtyIcon(bool isSet)
        {
            DirtyIconIsVisible = isSet;
        }

        private async Task WaitDialogFree(DialogInfoBase<object, string> dialogInfo)
        {
            //Console.WriteLine("WaitDialogFree,start,Busy:" + dialogInfo.Busy.ToString());
            while (dialogInfo.Busy)
            {
                await Task.Delay(1000);
                //Console.WriteLine("WaitDialogFree,loop,Busy:" + dialogInfo.Busy.ToString());
            }
            //Console.WriteLine("WaitDialogFree,end,Busy:" + dialogInfo.Busy.ToString());
        }
        #endregion utility

        #endregion Methods

        #endregion ViewModel
    }
}
