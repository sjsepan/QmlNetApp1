﻿//Program.cs
using Qml.Net;
using Qml.Net.Runtimes;

namespace QmlNetApp1
{
	static class Program
    {
        static int Main(string[] args)
        {
            RuntimeManager.DiscoverOrDownloadSuitableQtRuntime();
            // QQuickStyle.SetStyle("Material");//optional

            using (QGuiApplication guiApplication = new QGuiApplication(args))
            {
                using (QQmlApplicationEngine qmlApplicationEngine = new QQmlApplicationEngine())
                {
                    // Qml started throwing QtSettings error halfway through development
                    // after adding FileDialog, so am setting these now.
                    //https://github.com/qmlnet/qmlnet/issues/133
                    QCoreApplication.OrganizationDomain = "SJSepan";
                    QCoreApplication.OrganizationName = "sjsepan.com";

                    // Register our new type to be used in Qml
                    Qml.Net.Qml.RegisterType<MainWindowCode>("code", 1, 1);

                    qmlApplicationEngine.Load("MainWindow.qml");

                    return guiApplication.Exec();
                }
            }
        }
    }
}