//MainWindow.qml
import QtQuick 2.9
import QtQuick.Dialogs 1.3
import QtQuick.Controls 1.4 as QQC1
import QtQuick.Controls 2.3 
import QtQuick.Controls.Material 2.1
import QtQuick.Controls.Styles.Desktop 1.0
import QtQuick.Extras 1.4
import QtQuick.Layouts 1.3
import QtQuick.Window 2.10
import QtGraphicalEffects 1.0
import code 1.1
// import messageDialogInfo 1.1

ApplicationWindow 
{
    id: mainWindow
    width: 1024
    height: 480
    visible: true
    title: qsTr("QmlNetApp1")
    // icon.source: "./Resources/App.png"
    onClosing: 
    {
        // console.log("onClosing");
         // We can invoke async tasks that have continuation on the UI thread
         // https://discoverdot.net/projects/qml-net
        var resultOuter = false;
        var task = code.fileQuitAction();
        // And we can await the task
        Net.await(task, function(resultInner) 
        {
            // With the resultInner!
            // console.log("resultInner:" + resultInner);
            resultOuter = resultInner;
            if (resultInner)
            {
                //2) Qml ignores this too
                App.quit()
            }
        });
        //1) Note: this runs before await returns, but moving this inside causes handler
        // to NOT wait before ending
        // console.log("resultOuter:" + resultOuter);
        close.accepted=resultOuter;
    }

    // Material.theme: Material.Light
    // Material.primary: Material.Blue
    // Material.accent: Material.Blue

    //signal doSomeEvent(ParamType param)
    //onTargetChanged: doSomeEvent.connect(target.handleDoSomeEvent)
    
    MenuBar //TODO:consider assigning this to mainWindows's menuBar; No, obsolete
    {
        id: menuBar
        anchors.top: parent.top
        anchors.left: parent.left
        anchors.right: parent.right
        font.pixelSize: 12

        Menu 
        { 
            id: menuFile
            title: qsTr("&File")
            font.pixelSize: 12

            Action 
            {
                id: fileNewAction
                text: qsTr("&New\tCtrl-N")
                // icon.name: "file-new"
                icon.source: "./Resources/New.png"
                icon.color: "transparent"
                shortcut: StandardKey.New
                onTriggered: code.fileNew_Triggered("New")
                
            }
            Action 
            {
                id: fileOpenAction
                text: qsTr("&Open...\tCtrl-O")
                // icon.name: "file-open"
                icon.source: "./Resources/Open.png"
                icon.color: "transparent"
                shortcut: StandardKey.Open
                onTriggered: code.fileOpen_Triggered("Open")
            }
            Action 
            {
                id: fileSaveAction
                text: qsTr("&Save\tCtrl-S")
                // icon.name: "file-save"
                icon.source: "./Resources/Save.png"
                icon.color: "transparent"
                shortcut: StandardKey.Save
                onTriggered: code.fileSave_Triggered("Save")
            }
            Action 
            {
                id: fileSaveAsAction
                text: qsTr("Save &As..")
                // icon.name: "file-saveAs"
                icon.source: "./Resources/_Blank.png"
                icon.color: "transparent"
                shortcut: StandardKey.SaveAs
                onTriggered: code.fileSaveAs_Triggered("SaveAs")
            }

            MenuSeparator { }            
            
            Action 
            {
                id: filePrintAction
                text: qsTr("&Print\tCtrl-P")
                // icon.name: "file-print"
                icon.source: "./Resources/Print.png"
                icon.color: "transparent"
                shortcut: StandardKey.Print
                onTriggered: code.filePrint_Triggered("Print")
            }
            Action 
            {
                id: filePrintPreviewAction
                text: qsTr("P&rint Preview...")
                // icon.name: "file-printPreview"
                icon.source: "./Resources/_Blank.png"
                icon.color: "transparent"
                // shortcut: StandardKey.PrintPreview
                onTriggered: code.filePrintPreview_Triggered("PrintPreview")
            }

            MenuSeparator { }            
            
            Action 
            {
                id: fileQuitAction
                text: qsTr("&Quit\tCtrl-W") 
                icon.source: "./Resources/_Blank.png"
                icon.color: "transparent"
                // icon.color: "transparent"
                shortcut: StandardKey.Close
                onTriggered: /*console.log("fileQuitAction.onTriggered"),*/ mainWindow.close()
            }
        }
        Menu 
        { 
            id: menuEdit
            title: qsTr("&Edit")
            font.pixelSize: 12

            Action 
            {
                id: editUndoAction
                text: qsTr("&Undo\tCtrl-Z")
                // icon.name: "edit-undo"
                icon.source: "./Resources/Undo.png"
                icon.color: "transparent"
                shortcut: StandardKey.Undo
                onTriggered: code.editUndo_Triggered("undo")
            }
            Action 
            {
                id: editRedoAction
                text: qsTr("&Redo\tCtrl-Y")
                // icon.name: "edit-redo"
                icon.source: "./Resources/Redo.png"
                icon.color: "transparent"
                shortcut: StandardKey.Redo
                onTriggered: code.editRedo_Triggered("redo")
            }

            MenuSeparator { }            
            
            Action 
            {
                id: editSelectAllAction
                text: qsTr("Select &All\tCtrl-A")
                // icon.name: "edit-selectAll"
                icon.source: "./Resources/_Blank.png"
                icon.color: "transparent"
                shortcut: StandardKey.SelectAll
                onTriggered: code.editSelectAll_Triggered("selectAll")
            }
            Action 
            {
                id: editCutAction
                text: qsTr("Cu&t \tCtrl-X")
                // icon.name: "edit-cut"
                icon.source: "./Resources/Cut.png"
                icon.color: "transparent"
                shortcut: StandardKey.Cut
                onTriggered: code.editCut_Triggered("cut")
            }
            Action 
            {
                id: editCopyAction
                text: qsTr("C&opy\tCtrl-C")
                // icon.name: "edit-copy"
                icon.source: "./Resources/Copy.png"
                icon.color: "transparent"
                shortcut: StandardKey.Copy
                onTriggered: code.editCopy_Triggered("copy")
            }
            Action 
            {
                id: editPasteAction
                text: qsTr("&Paste\tCtrl-V")
                // icon.name: "edit-paste"
                icon.source: "./Resources/Paste.png"
                icon.color: "transparent"
                shortcut: StandardKey.Paste
                onTriggered: code.editPaste_Triggered("paste")
            }
            Action 
            {
                id: editPasteSpecialAction
                text: qsTr("Paste &Special")
                // icon.name: "edit-pastespecial"
                // icon.source: "./Resources/Paste.png"
                // icon.color: "transparent"
                // shortcut: StandardKey.PasteSpecial
                onTriggered: code.editPasteSpecial_Triggered("pasteSpecial")
            }
            Action 
            {
                id: editDeleteAction
                text: qsTr("&Delete\tDel")
                // icon.name: "edit-delete"
                icon.source: "./Resources/Delete.png"
                icon.color: "transparent"
                shortcut: StandardKey.Delete
                onTriggered: code.editDelete_Triggered("delete")
            }

            MenuSeparator { }            
            
            Action 
            {
                id: editFindAction
                text: qsTr("F&ind\tCtrl-F")
                // icon.name: "edit-find"
                icon.source: "./Resources/Find.png"
                icon.color: "transparent"
                shortcut: StandardKey.Find
                onTriggered: code.editFind_Triggered("find")
            }
            Action 
            {
                id: editReplaceAction
                text: qsTr("R&eplace...\tCtrl-H")
                // icon.name: "edit-replace"
                icon.source: "./Resources/Replace.png"
                icon.color: "transparent"
                shortcut: StandardKey.Replace
                onTriggered: code.editReplace_Triggered("replace")
            }

            MenuSeparator { }            
            
            Action 
            {
                id: editRefreshAction
                text: qsTr("Re&fresh\tF5")
                // icon.name: "edit-refresh"
                icon.source: "./Resources/Reload.png"
                icon.color: "transparent"
                shortcut: StandardKey.Refresh
                onTriggered: code.editRefresh_Triggered("refresh")
            }

            MenuSeparator { }            
            
            Action 
            {
                id: editPreferencesAction
                text: qsTr("Prefere&nces...")
                // icon.name: "edit-preferences"
                icon.source: "./Resources/Preferences.png"
                icon.color: "transparent"
                shortcut: StandardKey.Preferences
                onTriggered: code.editPreferences_Triggered("preferences")
            }
            Action 
            {
                id: editPropertiesAction
                text: qsTr("Properties...")
                // icon.name: "edit-properties"
                icon.source: "./Resources/Properties.png"
                icon.color: "transparent"
                // shortcut: StandardKey.Properties
                onTriggered: code.editProperties_Triggered("properties")
            }
        }
        Menu 
        { 
            id: menuWindow
            title: qsTr("&Window")
            font.pixelSize: 12

            Action 
            {
                id: windowNewWindowAction
                text: qsTr("&New Window")
                // icon.name: "window-newwindow"
                icon.source: "./Resources/_Blank.png"
                icon.color: "transparent"
                // shortcut: StandardKey.NewWindow
                onTriggered: code.windowNewWindow_Triggered("newwindow")

            } 
            Action 
            {
                id: windowTileAction
                text: qsTr("&Tile")
                // icon.name: "window-tile"
                icon.source: "./Resources/_Blank.png"
                icon.color: "transparent"
                // shortcut: StandardKey.Tile
                onTriggered: code.windowTile_Triggered("tile")

            } 
            Action 
            {
                id: windowCascadeAction
                text: qsTr("&Cascade")
                // icon.name: "window-cascade"
                icon.source: "./Resources/_Blank.png"
                icon.color: "transparent"
                // shortcut: StandardKey.Cascade
                onTriggered: code.windowCascade_Triggered("cascade")

            } 
            Action 
            {
                id: windowArrangeAllAction
                text: qsTr("&Arrange All")
                // icon.name: "window-arrangeall"
                icon.source: "./Resources/_Blank.png"
                icon.color: "transparent"
                // shortcut: StandardKey.ArrangeAll
                onTriggered: code.windowArrangeAll_Triggered("arrangeall")

            } 

            MenuSeparator { }            
            
            Action 
            {
                id: windowHideAction
                text: qsTr("&Hide")
                // icon.name: "window-hide"
                icon.source: "./Resources/_Blank.png"
                icon.color: "transparent"
                // shortcut: StandardKey.Hide
                onTriggered: code.windowHide_Triggered("hide")

            } 
            Action 
            {
                id: windowShowAction
                text: qsTr("&Show")
                // icon.name: "window-show"
                icon.source: "./Resources/_Blank.png"
                icon.color: "transparent"
                // shortcut: StandardKey.Show
                onTriggered: code.windowShow_Triggered("show")

            } 
        }
        Menu 
        { 
            id: menuHelp
            title: qsTr("&Help")
            font.pixelSize: 12
            
            Action 
            {
                id: helpContentsAction
                text: qsTr("&Contents\tF1")
                // icon.name: "help-help"
                icon.source: "./Resources/Contents.png"
                icon.color: "transparent"
                shortcut: StandardKey.HelpContents
                onTriggered: code.helpContents_Triggered("help")

            } 
            Action 
            {
                id: helpIndexAction
                text: qsTr("&Index")
                // icon.name: "help-index"
                icon.source: "./Resources/_Blank.png"
                icon.color: "transparent"
                // shortcut: StandardKey.Index
                onTriggered: code.helpIndex_Triggered("index")

            } 
            Action 
            {
                id: helpOnlineHelpAction
                text: qsTr("&Online Help")
                // icon.name: "help-onlinehelp"
                icon.source: "./Resources/_Blank.png"
                icon.color: "transparent"
                // shortcut: StandardKey.OnlineHelp
                onTriggered: code.helpOnlineHelp_Triggered("onlineHelp")

            } 

            MenuSeparator { }            
            
            Action 
            {
                id: helpLicenseInformationAction
                text: qsTr("&License Information")
                // icon.name: "help-licenseInformation"
                icon.source: "./Resources/_Blank.png"
                icon.color: "transparent"
                // shortcut: StandardKey.LicenseInformation
                onTriggered: code.helpLicenceInformation_Triggered("licenseInformation")

            } 
            Action 
            {
                id: helpCheckForUpdatesAction
                text: qsTr("Check for &Updates")
                // icon.name: "help-checkForUpdates"
                icon.source: "./Resources/_Blank.png"
                icon.color: "transparent"
                // shortcut: StandardKey.Check for Updates
                onTriggered: code.helpCheckForUpdates_Triggered("checkForUpdates")

            } 

            MenuSeparator { }            
            
            Action 
            {
                id: helpAboutAction
                text: qsTr("&About...")
                // icon.name: "help-about"
                icon.source: "./Resources/About.png"
                icon.color: "transparent"
                // shortcut: StandardKey.About
                onTriggered: code.helpAbout_Triggered("About") 
            } 
        }
    }

    ToolBar //TODO:consider assigning this to mainWindows's header
    {
        id: toolBar
        anchors.top: menuBar.bottom
        anchors.left: parent.left
        anchors.right: parent.right
        // Material.foreground: "white"

        RowLayout 
        {
            spacing: 2
            //anchors.fill: parent

            ToolButton 
            {
                // text: qsTr("New")
                icon.source: "./Resources/New.png"
                onClicked: /*console.log("New"),*/ code.fileNew_Triggered("New")
                ToolTip.visible: hovered
                ToolTip.text: qsTr("New")
                }
            ToolButton 
            {
                // text: qsTr("Open")
                icon.source: "./Resources/Open.png"
                onClicked: /*console.log("Open"),*/ code.fileOpen_Triggered("Open")
                ToolTip.visible: hovered
                ToolTip.text: qsTr("Open")
            }
            ToolButton 
            {
                // text: qsTr("Save")
                icon.source: "./Resources/Save.png"
                onClicked: /*console.log("Save"),*/ code.fileSave_Triggered("save")
                ToolTip.visible: hovered
                ToolTip.text: qsTr("Save")
            }
            ToolButton 
            {
                // text: qsTr("Print")
                icon.source: "./Resources/Print.png"
                onClicked: /*console.log("Print"),*/ code.filePrint_Triggered("print")
                ToolTip.visible: hovered
                ToolTip.text: qsTr("Print")
            }

            ToolSeparator {}

            ToolButton 
            {
                // text: qsTr("Undo")
                icon.source: "./Resources/Undo.png"
                onClicked: /*console.log("Undo"),*/ code.editUndo_Triggered("undo")
                ToolTip.visible: hovered
                ToolTip.text: qsTr("Undo")
            }
            ToolButton 
            {
                // text: qsTr("Redo")
                icon.source: "./Resources/Redo.png"
                onClicked: /*console.log("Redo"),*/ code.editRedo_Triggered("redo")
                ToolTip.visible: hovered
                ToolTip.text: qsTr("Redo")
            }
            ToolButton 
            {
                // text: qsTr("Cut")
                icon.source: "./Resources/Cut.png"
                onClicked: /*console.log("Cut"),*/ code.editCut_Triggered("cut")
                ToolTip.visible: hovered
                ToolTip.text: qsTr("Cut")
            }
            ToolButton 
            {
                // text: qsTr("Copy")
                icon.source: "./Resources/Copy.png"
                onClicked: /*console.log("Copy"),*/ code.editCopy_Triggered("copy")
                ToolTip.visible: hovered
                ToolTip.text: qsTr("Copy")
            }
            ToolButton 
            {
                // text: qsTr("Paste")
                icon.source: "./Resources/Paste.png"
                onClicked: /*console.log("Paste"),*/ code.editPaste_Triggered("paste")
                ToolTip.visible: hovered
                ToolTip.text: qsTr("Paste")
            }
            ToolButton 
            {
                // text: qsTr("Delete")
                icon.source: "./Resources/Delete.png"
                onClicked: /*console.log("Delete"),*/ code.editDelete_Triggered("delete")
                ToolTip.visible: hovered
                ToolTip.text: qsTr("Delete")
            }
            ToolButton 
            {
                // text: qsTr("Find")
                icon.source: "./Resources/Find.png"
                onClicked: /*console.log("Find"),*/ code.editFind_Triggered("find")
                ToolTip.visible: hovered
                ToolTip.text: qsTr("Find")
            }
            ToolButton 
            {
                // text: qsTr("Replace")
                icon.source: "./Resources/Replace.png"
                onClicked: /*console.log("Replace"),*/ code.editReplace_Triggered("replace")
                ToolTip.visible: hovered
                ToolTip.text: qsTr("Replace")
            }
            ToolButton 
            {
                // text: qsTr("Refresh")
                icon.source: "./Resources/Reload.png"
                onClicked: /*console.log("Refresh"),*/ code.editRefresh_Triggered("refresh")
                ToolTip.visible: hovered
                ToolTip.text: qsTr("Refresh")
            }
            ToolButton 
            {
                // text: qsTr("Preferences")
                icon.source: "./Resources/Preferences.png"
                onClicked: /*console.log("Preferences"),*/ code.editPreferences_Triggered("preferences")
                ToolTip.visible: hovered
                ToolTip.text: qsTr("Preferences")
            }
            ToolButton 
            {
                // text: qsTr("Properties")
                icon.source: "./Resources/Properties.png"
                onClicked: /*console.log("Properties"),*/ code.editProperties_Triggered("properties")
                ToolTip.visible: hovered
                ToolTip.text: qsTr("Properties")
            }
            
            ToolSeparator {}

            ToolButton 
            {
                // text: qsTr("Help")
                icon.source: "./Resources/Contents.png"
                onClicked: /*console.log("Help"),*/ code.helpContents_Triggered("help")
                ToolTip.visible: hovered
                ToolTip.text: qsTr("Help")
            }
        }
    }

    Grid
    {
        id: grid
        anchors.top: toolBar.bottom
        anchors.bottom: statusBar.top
        anchors.left: parent.left
        anchors.right: parent.right
        padding: 4
        spacing: 2
        columns: 7
        rows: 4
 
        Label
        {
            id: lblSomeInt
            text: "Some Integer:"
            font.pixelSize: 12
            //elide: Label.ElideRight
            horizontalAlignment: Qt.AlignHRight
            verticalAlignment: Qt.AlignVCenter
            Layout.fillWidth: false
            Layout.row: 0
            Layout.column: 0

        }

        TextField
        {
            id: txtSomeInt
            // x: 22
            // y: 126
            // width: 80
            height: 25
            text: qsTr("txtSomeInt")
            onAccepted: code.someInt = txtSomeInt.text
            font.pixelSize: 12
            Layout.fillWidth: false
            Layout.row: 0
            Layout.column: 1
        }

        Label
        {
            id: lblSomeOtherInt
            text: "Other Integer:"
            font.pixelSize: 12
            //elide: Label.ElideRight
            // horizontalAlignment: Qt.AlignHCenter
            // verticalAlignment: Qt.AlignVCenter
            Layout.fillWidth: false
            Layout.row: 0
            Layout.column: 2

        }

        TextField
        {
            id: txtSomeOtherInt
            // x: 22
            // y: 126
            // width: 80
            height: 25
            text: qsTr("txtSomeOtherInt")
            onAccepted: code.someOtherInt = txtSomeOtherInt.text
            font.pixelSize: 12
            Layout.fillWidth: false
            Layout.row: 0
            Layout.column: 3
        }

        Label
        {
            id: lblStillAnotherInt
            text: "Another Integer:"
            font.pixelSize: 12
            //elide: Label.ElideRight
            // horizontalAlignment: Qt.AlignHCenter
            // verticalAlignment: Qt.AlignVCenter
            Layout.fillWidth: false
            Layout.row: 0
            Layout.column: 4

        }

        TextField
        {
            id: txtStillAnotherInt
            // x: 22
            // y: 126
            // width: 80
            height: 25
            text: qsTr("txtStillAnotherInt")
            onAccepted: code.stillAnotherInt = txtStillAnotherInt.text
            font.pixelSize: 12
            Layout.fillWidth: false
            Layout.row: 0
            Layout.column: 5
        }
 
        Button
        {
            id: cmdRun
            text: "&Run"
            onClicked: /*console.log("Run"),*/ code.cmdRun_Clicked("Run")
            Layout.fillWidth: false
            Layout.row: 0
            Layout.column: 6
        }

        Label
        {
            id: lblSomeString
            text: "Some String:"
            font.pixelSize: 12
            //elide: Label.ElideRight
            // horizontalAlignment: Qt.AlignHCenter
            // verticalAlignment: Qt.AlignVCenter
            Layout.fillWidth: false
            Layout.row: 1
            Layout.column: 0

        }

        TextField
        {
            id: txtSomeString
            // x: 22
            // y: 126
            // width: 80
            height: 25
            horizontalAlignment: Qt.AlignHRight
            verticalAlignment: Qt.AlignVCenter
            text: qsTr("txtSomeString")
            onAccepted: code.someString = txtSomeString.text
            font.pixelSize: 12
            Layout.fillWidth: false
            Layout.row: 1
            Layout.column: 1
        }

        Label
        {
            id: lblSomeOtherString
            text: "Other String:"
            font.pixelSize: 12
            //elide: Label.ElideRight
            // horizontalAlignment: Qt.AlignHCenter
            // verticalAlignment: Qt.AlignVCenter
            Layout.fillWidth: false
            Layout.row: 1
            Layout.column: 2

        }

        TextField
        {
            id: txtSomeOtherString
            // x: 22
            // y: 126
            // width: 80
            height: 25
            text: qsTr("txtSomeOtherString")
            onAccepted: code.someOtherString = txtSomeOtherString.text
            font.pixelSize: 12
            Layout.fillWidth: false
            Layout.row: 1
            Layout.column: 3
        }

        Label
        {
            id: lblStillAnotherString
            text: "Another String:"
            font.pixelSize: 12
            //elide: Label.ElideRight
            // horizontalAlignment: Qt.AlignHCenter
            // verticalAlignment: Qt.AlignVCenter
            Layout.fillWidth: false
            Layout.row: 1
            Layout.column: 4

        }

        TextField
        {
            id: txtStillAnotherString
            // x: 22
            // y: 126
            // width: 80
            height: 25
            text: qsTr("txtStillAnotherString")
            onAccepted: code.stillAnotherString = txtStillAnotherString.text
            font.pixelSize: 12
            Layout.fillWidth: false
            Layout.row: 1
            Layout.column: 5
        }
 
        Label
        {
            id: lblSpacer1
            text: " "
            Layout.fillWidth: false
            Layout.row: 0
            Layout.column: 6
        }

        Label
        {
            id: lblSomeBoolean
            text: "Some Boolean:"
            font.pixelSize: 12
            //elide: Label.ElideRight
            // horizontalAlignment: Qt.AlignHCenter
            // verticalAlignment: Qt.AlignVCenter
            Layout.fillWidth: false
            Layout.row: 2
            Layout.column: 0

        }

        CheckBox
        {
            id: chkSomeBoolean
            height: 25
            text: ""//qsTr("chkSomeBoolean")
            onCheckStateChanged: code.someBoolean = chkSomeBoolean.checked
            Layout.fillWidth: false
            Layout.row: 2
            Layout.column: 1
        }

        Label
        {
            id: lblSomeOtherBoolean
            text: "Other Boolean:"
            font.pixelSize: 12
            //elide: Label.ElideRight
            // horizontalAlignment: Qt.AlignHCenter
            // verticalAlignment: Qt.AlignVCenter
            Layout.fillWidth: false
            Layout.row: 2
            Layout.column: 2

        }

        CheckBox
        {
            id: chkSomeOtherBoolean
            height: 25
            text: ""//qsTr("chkSomeOtherBoolean")
            onCheckStateChanged: code.someOtherBoolean = chkSomeOtherBoolean.checked
            Layout.fillWidth: false
            Layout.row: 2
            Layout.column: 3
        }

        Label
        {
            id: lblStillAnotherBoolean
            text: "Another Boolean:"
            font.pixelSize: 12
            //elide: Label.ElideRight
            // horizontalAlignment: Qt.AlignHCenter
            // verticalAlignment: Qt.AlignVCenter
            Layout.fillWidth: false
            Layout.row: 2
            Layout.column: 4

        }
    
        CheckBox    
        {
            id: chkStillAnotherBoolean
            height: 25
            text: ""//qsTr("chkStillAnotherBoolean")
            onCheckStateChanged: code.stillAnotherBoolean = chkStillAnotherBoolean.checked
            Layout.fillWidth: false
            Layout.row: 2
            Layout.column: 5
        }
 
        Label
        {
            id: lblSpacer2
            text: " "
            Layout.fillWidth: false
            Layout.row: 2
            Layout.column: 6
        }

        Button
        {
            id: cmdFont
            text: "&Font"
            onClicked: /*console.log("Font"),*/ code.cmdFont_Clicked("Font")
            Layout.fillWidth: false
            Layout.row: 3
            Layout.column: 0
        }


        Button
        {
            id: cmdColor
            text: "&Color"
            onClicked: /*console.log("Color"),*/ code.cmdColor_Clicked("Color")
            Layout.fillWidth: false
            Layout.row: 3
            Layout.column: 1
        }
    }

    QQC1.StatusBar //TODO:consider assigning this to mainWindows's footer (if allowed; use Item to wrap otherwise)
    {
        id: statusBar
        anchors.bottom: parent.bottom
        anchors.left: parent.left
        anchors.right: parent.right

        RowLayout 
        {
            //anchors.fill: parent
            spacing: 2

            Label 
            { 
                id: statusMessage
                text: "" 
                color: "Green"
            }
            Label 
            { 
                id: errorMessage
                text: "" 
                color: "Red"
            }
            ProgressBar 
            {
                id: progressBar
                // value: 0.5
                indeterminate: true
                visible: false
            }   
            Image
            {
                id: actionIcon
                source: "./Resources/New.png"
                sourceSize.width: 22
                sourceSize.height: 22
                visible: false
            }
            Image
            {
                id: dirtyIcon
                source: "./Resources/Save.png"
                sourceSize.width: 22
                sourceSize.height: 22
                visible: false
            }
        }
    }

    MainWindowCode
    {
        id: code

        Component.onCompleted: function()
        {
        }

        onShowMessageDialog: function(dialogInfoType) 
        {
            // console.log("dialogInfoType:" + dialogInfoType)
            // console.log("code.msgDialogInfo.title:" + code.msgDialogInfo.title)
            if (code.msgDialogInfo.modal)
            {
                messageDialog.modality =  Qt.WindowModal
            }
            else
            {
                messageDialog.modality =  Qt.NonModal
            }
            // console.log("code.msgDialogInfo.message.length:"+ code.msgDialogInfo.message.length)
            if (code.msgDialogInfo.title != null)
            {
                messageDialog.title = code.msgDialogInfo.title
            }//else use window title hard-coded
            messageDialog.text = code.msgDialogInfo.message
            messageDialog.informativeText = code.msgDialogInfo.message2 
            messageDialog.detailedText = code.msgDialogInfo.message3 
            // console.log("code.msgDialogInfo.buttonsType:"+ code.msgDialogInfo.buttonsType)
            switch (code.msgDialogInfo.buttonsType)
            {
                case "YesNo":
                    messageDialog.standardButtons = StandardButton.Yes | StandardButton.No
                    break;
                case "OkCancel":
                    messageDialog.standardButtons = StandardButton.Ok | StandardButton.Cancel
                    break;
                default:
                    messageDialog.standardButtons = StandardButton.Ok
            }
            switch (code.msgDialogInfo.messageType)
            {
                case "Question":
                    messageDialog.icon = StandardIcon.Question
                    break;
                case "Information":
                    messageDialog.icon = StandardIcon.Information
                    break;
                case "Warning":
                    messageDialog.icon = StandardIcon.Warning
                    break;
                case "Critical":
                    messageDialog.icon = StandardIcon.Critical
                    break;
                default:
                    messageDialog.icon = StandardIcon.NoIcon
            }
            messageDialog.visible = true
        }

        //very similar to onShowMessageDialog, but must use AboutDialogInfo
        onShowAboutDialog: function(dialogInfoType) 
        {
            // console.log("dialogInfoType:" + dialogInfoType)
            // console.log("code.abtDialogInfo.title:" + code.abtDialogInfo.title)
            if (code.abtDialogInfo.modal)
            {
                aboutDialog.modality =  Qt.WindowModal
            }
            else
            {
                aboutDialog.modality =  Qt.NonModal
            }
            // console.log("code.abtDialogInfo.message.length:"+ code.abtDialogInfo.message.length)
            if (code.abtDialogInfo.title != null)
            {
                aboutDialog.title = code.abtDialogInfo.title
            }//else use window title hard-coded
            aboutDialog.text = code.abtDialogInfo.comments
            aboutDialog.informativeText = "";//code.abtDialogInfo.message2 
            aboutDialog.detailedText = ""; //code.abtDialogInfo.message3 
            // console.log("code.abtDialogInfo.buttonsType:"+ code.abtDialogInfo.buttonsType)
            aboutDialog.standardButtons = StandardButton.Ok
            aboutDialog.icon = StandardIcon.Information
            aboutDialog.visible = true
        }
        
        onShowFontDialog: function(dialogInfoType) 
        {
            // console.log("dialogInfoType:" + dialogInfoType)
            fontDialog.visible = true
        }
        
        onShowColorDialog: function(dialogInfoType) 
        {
            // console.log("dialogInfoType:" + dialogInfoType)
            colorDialog.visible = true
        }
        
        onShowFileDialog: function(dialogInfoType) 
        {
            // console.log("dialogInfoType:" + dialogInfoType)
            if (code.filDialogInfo.modal)
            {
                fileDialog.modality =  Qt.WindowModal
            }
            else
            {
                fileDialog.modality =  Qt.NonModal
            }
            if (code.filDialogInfo.title != null)
            {
                fileDialog.title = code.filDialogInfo.title
            }//else use window title hard-coded
            console.log("onShowFileDialog:code.filDialogInfo.Filters=" + code.filDialogInfo.filters);
            fileDialog.nameFilters = /*"All files (*)";*/code.filDialogInfo.filters;
            fileDialog.selectExisting = code.filDialogInfo.mustExist;
            fileDialog.selectFolder = code.filDialogInfo.selectFolders;
            fileDialog.selectMultiple = code.filDialogInfo.multiselect;
            // fileDialog.selectedNameFilter = code.filDialogInfo.TODO;
            fileDialog.visible = true
        }

        // onShowPrintDialog: function(dialogInfoType)
        // {
        //     console.log("dialogInfoType:" + dialogInfoType)
        // }

        onPropertyChanged: function(propertyName) 
        {
            switch(propertyName) 
            {
                //Fields
                case "SomeInt":
                    txtSomeInt.text = code.someInt
                    break;
                case "SomeString":
                    txtSomeString.text = code.someString
                    break;
                case "SomeBoolean":
                    chkSomeBoolean.checked = code.someBoolean
                    break;
                case "SomeOtherInt":
                    txtSomeOtherInt.text = code.someOtherInt
                    break;
                case "SomeOtherString":
                    txtSomeOtherString.text = code.someOtherString
                    break;
                case "SomeOtherBoolean":
                    chkSomeOtherBoolean.checked = code.someOtherBoolean
                    break;
                case "StillAnotherInt":
                    txtStillAnotherInt.text = code.stillAnotherInt
                    break;
                case "StillAnotherString":
                    txtStillAnotherString.text = code.stillAnotherString
                    break;
                case "StillAnotherBoolean":
                    chkStillAnotherBoolean.checked = code.stillAnotherBoolean
                    break;

                //Status Bar
                case "StatusMessage":
                    var s = code.statusMessage
                    // console.log("code.StatusMessage"+s)
                    statusMessage.text = s
                    break;
                case "ErrorMessage":
                    errorMessage.text = code.errorMessage;
                    break;
                case "ErrorMessageToolTipText":
                    // errorMessage.Tag = code.errorMessageToolTipText;
                    break;
                case "CustomMessage":
                    //StatusBarCustomMessage.text = code.CustomMessage;
                    break;
                case "ProgressBarValue":
                    progressBar.value = code.progressBarValue / 100;
                    break;
                case "ProgressBarMaximum":
                    progressBar.to = code.progressBarMaximum;
                    break;
                case "ProgressBarMinimum":
                    progressBar.from = code.progressBarMinimum;
                    break;
                case "ProgressBarStep":
                    // progressBar.SmallChange = code.progressBarStep;
                    break;
                case "ProgressBarIsMarquee":
                    progressBar.indeterminate = code.progressBarIsMarquee;
                    break;
                case "ProgressBarIsVisible":
                    progressBar.visible = code.progressBarIsVisible;
                    break;
                case "ActionIconIsVisible":
                    actionIcon.visible = code.actionIconIsVisible;
                    break;
                case "ActionIconImage":
                    actionIcon.source = code.actionIconImage;
                    break;
                case "DirtyIconIsVisible":
                    dirtyIcon.visible = code.dirtyIconIsVisible;
                    break;
                case "DirtyIconImage":
                    dirtyIcon.source = code.dirtyIconImage;
                    break;
                default:
                    console.log("code.onPropertyChanged" + propertyName)
            } 
        }
    }
    
    MessageDialog
    {
        id: messageDialog
        title: "QmlNetApp1" //Window title
        text: ""
        Component.onCompleted: 
        {
        }
        onYes:
        {
            // console.log("onYes");

            code.msgDialogInfo.boolResult = true;
            code.msgDialogInfo.response = "Yes";
            code.msgDialogInfo.errorMessage = "";
            
            // console.log("code.msgDialogInfo.busy.toString():" + code.msgDialogInfo.busy.toString());
            code.msgDialogInfo.busy = false;
            // console.log("code.msgDialogInfo.busy.toString():" + code.msgDialogInfo.busy.toString());
        }
        onAccepted: 
        {
            // console.log("onAccepted")

            code.msgDialogInfo.boolResult = true;
            code.msgDialogInfo.response = "Ok";
            code.msgDialogInfo.errorMessage = "";
            
            code.msgDialogInfo.busy = false;
        }
        onNo:
        {
            // console.log("onNo")

            code.msgDialogInfo.boolResult = true;
            code.msgDialogInfo.response = "No";
            code.msgDialogInfo.errorMessage = "";
            
            // console.log("code.msgDialogInfo.busy.toString():" + code.msgDialogInfo.busy.toString());
            code.msgDialogInfo.busy = false;
            // console.log("code.msgDialogInfo.busy.toString():" + code.msgDialogInfo.busy.toString());
        }
        onRejected: 
        {
            // console.log("onRejected")

            code.msgDialogInfo.boolResult = true;
            code.msgDialogInfo.response = "Cancel";
            code.msgDialogInfo.errorMessage = "";
            
            code.msgDialogInfo.busy = false;
        }
    }
    
    //about
    MessageDialog
    {
        id: aboutDialog
        title: "QmlNetApp1" //Window title
        text: ""
        Component.onCompleted: 
        {
        }
        onAccepted: 
        {
            // console.log("onAccepted")

            code.abtDialogInfo.boolResult = true;
            code.abtDialogInfo.response = "Ok";
            code.abtDialogInfo.errorMessage = "";
            
            code.abtDialogInfo.busy = false;
        }
    }

    FontDialog
    {
        id: fontDialog
        title: "QmlNetApp1" //Window title
        Component.onCompleted: 
        {
        }
        onAccepted: 
        {
            // console.log("onAccepted")

            code.fntDialogInfo.boolResult = true;
            code.fntDialogInfo.response = "Ok";
            code.fntDialogInfo.errorMessage = "";
            //font is complex object not recognized by .Net; pass string(s)
            code.fntDialogInfo.fontDescription = fontDialog.font.toString();
            
            code.fntDialogInfo.busy = false;
        }

        onRejected: 
        {
            // console.log("onRejected")

            code.fntDialogInfo.boolResult = true;
            code.fntDialogInfo.response = "Cancel";
            code.fntDialogInfo.errorMessage = "";
            
            code.fntDialogInfo.busy = false;
        }

    }

    ColorDialog
    {
        id: colorDialog
        title: "QmlNetApp1" //Window title
        Component.onCompleted: 
        {
        }
        onAccepted: 
        {
            // console.log("onAccepted")

            code.clrDialogInfo.boolResult = true;
            code.clrDialogInfo.response = "Ok";
            code.clrDialogInfo.errorMessage = "";
            code.clrDialogInfo.color = colorDialog.color;
            
            code.clrDialogInfo.busy = false;
        }

        onRejected: 
        {
            // console.log("onRejected")

            code.clrDialogInfo.boolResult = true;
            code.clrDialogInfo.response = "Cancel";
            code.clrDialogInfo.errorMessage = "";
            
            code.clrDialogInfo.busy = false;
        }

    }
    
    FileDialog
    {
        id: fileDialog
        visible: false //Note: recommended in https://github.com/qmlnet/qmlnet/issues/183
        title: "QmlNetApp1" //Window title
         selectMultiple: false
         selectExisting: true
         selectFolder: false
        // nameFilters: [ "Xxx files (*.xxx)" ]
        Component.onCompleted: 
        {
        }
        onAccepted: 
        {
            // console.log("onAccepted")

            code.filDialogInfo.boolResult = true;
            code.filDialogInfo.response = "Ok";
            code.filDialogInfo.errorMessage = "";
            if (code.filDialogInfo.selectFolders)
            {
                code.filDialogInfo.filename = fileDialog.folder;
            }
            else
            {
                // console.log("fileDialog.fileUrl:" + fileDialog.fileUrl);
                code.filDialogInfo.filename = fileDialog.fileUrl;
                // console.log("code.filDialogInfo.filename:" + code.filDialogInfo.filename);
                if (code.filDialogInfo.multiselect)
                {
                    code.filDialogInfo.filenames = file.fileUrls;
                }
            }
            
            code.filDialogInfo.busy = false;
        }

        onRejected: 
        {
            // console.log("onRejected")

            code.filDialogInfo.boolResult = true;
            code.filDialogInfo.response = "Cancel";
            code.filDialogInfo.errorMessage = "";
            
            code.filDialogInfo.busy = false;
        }
    }

    // PrintDialog
    // {
    //     id: printDialog
    // }
}
